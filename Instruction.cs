﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace x86asmref
{
    class Instruction
    {
        public class Operand
        {
            public bool bHasAt;
            public string a;
            public string t;
            public string other;

            public Operand()
            {
                bHasAt = true;
                a = "";
                t = "";
                other = "";
            }
        }

        public string mnemonic;
        public List<Operand> operands;

        public Instruction()
        {
            this.operands = new List<Operand>();
            mnemonic = "";
        }

        public bool InstructionFromSyntax(syntax curSyntax)
        {
            if (curSyntax.mnem == null)
                return false;
            if (curSyntax.mnem.sugSpecified) //undocumented instructions
                return false;
            this.mnemonic = curSyntax.mnem.Value;

            if (curSyntax.dst != null) //list destination (when present)
            {
                for (int i = 0; i < curSyntax.dst.GetLength(0); i++)
                {
                    if (curSyntax.dst[i].displayedSpecified && curSyntax.dst[i].displayed == dstDisplayed.no)
                        continue;
                    if (curSyntax.dst[i].Items != null)
                    {
                        Operand newOperand = new Operand();
                        for (int j = 0; j < curSyntax.dst[i].Items.GetLength(0); j++)
                        {
                            if (curSyntax.dst[i].Items[j] == null)
                                continue;
                            string ItemsElementName = curSyntax.dst[i].ItemsElementName[j].ToString();
                            if (ItemsElementName.Equals("a"))
                                newOperand.a = curSyntax.dst[i].Items[j];
                            else if (ItemsElementName.Equals("t"))
                                newOperand.t = curSyntax.dst[i].Items[j];
                        }
                        this.operands.Add(newOperand);
                    }
                    else if (curSyntax.dst[i].Text != null)
                    {
                        string a = "";
                        string t = "";

                        bool bHasAt = false;

                        if (curSyntax.dst[i].type != null)
                        {
                            bHasAt = true;
                            t = curSyntax.dst[i].type;
                        }
                        if (curSyntax.dst[i].addressSpecified)
                        {
                            bHasAt = true;
                            a = curSyntax.dst[i].address.ToString();
                        }
                        Operand newOperand = new Operand();
                        if (!bHasAt)
                        {
                            for (int j = 0; j < curSyntax.dst[i].Text.GetLength(0); j++)
                            {
                                if (curSyntax.dst[i].Text[j] == null)
                                    continue;
                                newOperand.other = curSyntax.dst[i].Text[j];
                                newOperand.bHasAt = false;
                            }
                        }
                        else
                        {
                            newOperand.a = a;
                            newOperand.t = t;
                        }
                        this.operands.Add(newOperand);
                    }
                }
            }
            if (curSyntax.src != null) //list source (when present)
            {
                for (int i = 0; i < curSyntax.src.GetLength(0); i++)
                {
                    if (curSyntax.src[i].displayedSpecified && curSyntax.src[i].displayed == srcDisplayed.no)
                        continue;
                    if (curSyntax.src[i].Items != null)
                    {
                        Operand newOperand = new Operand();
                        for (int j = 0; j < curSyntax.src[i].Items.GetLength(0); j++)
                        {
                            if (curSyntax.src[i].Items[j] == null)
                                continue;
                            string ItemsElementName = curSyntax.src[i].ItemsElementName[j].ToString();
                            if (ItemsElementName.Equals("a"))
                                newOperand.a = curSyntax.src[i].Items[j];
                            else if (ItemsElementName.Equals("t"))
                                newOperand.t = curSyntax.src[i].Items[j];
                        }
                        this.operands.Add(newOperand);
                    }
                    else if (curSyntax.src[i].Text != null)
                    {
                        string a = "";
                        string t = "";

                        bool bHasAt = false;

                        if (curSyntax.src[i].type != null)
                        {
                            bHasAt = true;
                            t = curSyntax.src[i].type;
                        }
                        if (curSyntax.src[i].addressSpecified)
                        {
                            bHasAt = true;
                            a = curSyntax.src[i].address.ToString();
                        }
                        Operand newOperand = new Operand();
                        if (!bHasAt)
                        {
                            for (int j = 0; j < curSyntax.src[i].Text.GetLength(0); j++)
                            {
                                if (curSyntax.src[i].Text[j] == null)
                                    continue;
                                newOperand.other = curSyntax.src[i].Text[j];
                                newOperand.bHasAt = false;
                            }
                        }
                        else
                        {
                            newOperand.a = a;
                            newOperand.t = t;
                        }
                        this.operands.Add(newOperand);
                    }
                }
            }
            return true;
        }

        public static void GetInstructions(pri_opcd[] curPriopcd, List<Instruction> instrList)
        {
            foreach (pri_opcd onebyte in curPriopcd)
            {
                foreach (entry curEntry in onebyte.entry)
                {
                    //skip prefixes
                    if (curEntry.grp1 != null && curEntry.grp1.Equals("prefix"))
                        continue;
                    if (curEntry.grp2 != null && curEntry.grp2.Equals("prefix"))
                        continue;
                    if (curEntry.grp3 != null && curEntry.grp3.Equals("prefix"))
                        continue;

                    foreach (syntax curSyntax in curEntry.syntax)
                    {
                        Instruction curInstr = new Instruction();
                        if (!curInstr.InstructionFromSyntax(curSyntax))
                            continue;
                        instrList.Add(curInstr);
                    }
                }
            }
        }

        public static x86reference DeserializeReference(XmlDocument doc)
        {
            //initialize deserializer
            XmlSerializer deserializer = new XmlSerializer(typeof(x86reference));

            //remove all comments
            XmlNodeList commentList = doc.SelectNodes("//comment()");
            foreach (XmlNode node in commentList)
                node.ParentNode.RemoveChild(node);

            //store to memory stream and rewind
            MemoryStream ms = new MemoryStream();
            doc.Save(ms);
            ms.Seek(0, SeekOrigin.Begin);

            //deserialize using clean xml
            return (x86reference)deserializer.Deserialize(XmlReader.Create(ms));
        }
    }
}
