﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace x86asmref
{
    public partial class Form1 : Form
    {
        private string header = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //load xml file
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(@"x86reference.xml");
            }
            catch
            {
                try
                {
                    doc.Load(@"..\..\x86reference.xml");
                }
                catch
                {
                    MessageBox.Show("x86reference.xml not found!");
                    return;
                }
            }

            //deserialize the reference
            x86reference reference = Instruction.DeserializeReference(doc);

            //set the document version label
            label1.Text = "Reference Version: " + reference.version;

            //create an instruction list
            List<Instruction> instrList = new List<Instruction>();

            Instruction.GetInstructions(reference.onebyte, instrList);
            Instruction.GetInstructions(reference.twobyte.pri_opcd, instrList);

            //clear the listbox
            listBox1.Items.Clear();

            //reset the header
            header = "#define XED_ICLASS_RETN XED_ICLASS_RET_NEAR\r\n#define XED_ICLASS_RETF XED_ICLASS_RET_FAR\r\n#define XED_ICLASS_CALL XED_ICLASS_CALL_NEAR\r\n#define XED_ICLASS_CALLF XED_ICLASS_CALL_FAR\r\n#define XED_ICLASS_JMPF XED_ICLASS_JMP_FAR\r\n\r\n#define XED_ICLASS_JNAE XED_ICLASS_JB\r\n#define XED_ICLASS_JC XED_ICLASS_JB\r\n#define XED_ICLASS_JAE XED_ICLASS_JNB\r\n#define XED_ICLASS_JNC XED_ICLASS_JNB\r\n#define XED_ICLASS_JE XED_ICLASS_JZ\r\n#define XED_ICLASS_JNE XED_ICLASS_JNZ\r\n#define XED_ICLASS_JNA XED_ICLASS_JBE\r\n#define XED_ICLASS_JA XED_ICLASS_JNBE\r\n#define XED_ICLASS_JPE XED_ICLASS_JP\r\n#define XED_ICLASS_JPO XED_ICLASS_JNP\r\n#define XED_ICLASS_JNGE XED_ICLASS_JL\r\n#define XED_ICLASS_JGE XED_ICLASS_JNL\r\n#define XED_ICLASS_JNG XED_ICLASS_JLE\r\n#define XED_ICLASS_JG XED_ICLASS_JNLE\r\n#define XED_ICLASS_JCXZ XED_ICLASS_JRCXZ\r\n#define XED_ICLASS_JECXZ XED_ICLASS_JRCXZ\r\n\r\n#define XED_ICLASS_LOOPZ XED_ICLASS_LOOPE\r\n#define XED_ICLASS_LOOPNZ XED_ICLASS_LOOPNE\r\n\r\n#define XED_ICLASS_CMOVAE XED_ICLASS_CMOVB\r\n#define XED_ICLASS_CMOVC XED_ICLASS_CMOVB\r\n#define XED_ICLASS_CMOVNC XED_ICLASS_CMOVNB\r\n#define XED_ICLASS_CMOVNAE XED_ICLASS_CMOVNB\r\n#define XED_ICLASS_CMOVE XED_ICLASS_CMOVZ\r\n#define XED_ICLASS_CMOVNE XED_ICLASS_CMOVNZ\r\n#define XED_ICLASS_CMOVNA XED_ICLASS_CMOVBE\r\n#define XED_ICLASS_CMOVA XED_ICLASS_CMOVNBE\r\n#define XED_ICLASS_CMOVPE XED_ICLASS_CMOVP\r\n#define XED_ICLASS_CMOVPO XED_ICLASS_CMOVNP\r\n#define XED_ICLASS_CMOVNGE XED_ICLASS_CMOVLE\r\n#define XED_ICLASS_CMOVGE XED_ICLASS_CMOVNL\r\n#define XED_ICLASS_CMOVNG XED_ICLASS_CMOVLE\r\n#define XED_ICLASS_CMOVG XED_ICLASS_CMOVNLE\r\n\r\n#define XED_ICLASS_SETNAE XED_ICLASS_SETNB\r\n#define XED_ICLASS_SETC XED_ICLASS_SETB\r\n#define XED_ICLASS_SETAE XED_ICLASS_SETB\r\n#define XED_ICLASS_SETNC XED_ICLASS_SETNB\r\n#define XED_ICLASS_SETE XED_ICLASS_SETZ\r\n#define XED_ICLASS_SETNE XED_ICLASS_SETNZ\r\n#define XED_ICLASS_SETNA XED_ICLASS_SETBE\r\n#define XED_ICLASS_SETA XED_ICLASS_SETNBE\r\n#define XED_ICLASS_SETPE XED_ICLASS_SETP\r\n#define XED_ICLASS_SETPO XED_ICLASS_SETP\r\n#define XED_ICLASS_SETNGE XED_ICLASS_SETLE\r\n#define XED_ICLASS_SETGE XED_ICLASS_SETNL\r\n#define XED_ICLASS_SETNG XED_ICLASS_SETLE\r\n#define XED_ICLASS_SETG XED_ICLASS_SETNLE\r\n\r\n#define XED_ICLASS_OUTS XED_ICLASS_OUT\r\n#define XED_ICLASS_INS XED_ICLASS_IN\r\n#define XED_ICLASS_WAIT XED_ICLASS_MWAIT\r\n#define XED_ICLASS_MOVS XED_ICLASS_MOV\r\n#define XED_ICLASS_CMPS XED_ICLASS_CMP\r\n#define XED_ICLASS_XLATB XED_ICLASS_XLAT\r\n#define XED_ICLASS_ICEBP XED_ICLASS_INT1\r\n\r\n#define XED_ICLASS_FSTENV XED_ICLASS_FNSTENV\r\n#define XED_ICLASS_FSTCW XED_ICLASS_FNSTCW\r\n#define XED_ICLASS_FCLEX XED_ICLASS_FNCLEX\r\n#define XED_ICLASS_FINIT XED_ICLASS_FNINIT\r\n#define XED_ICLASS_FSAVE XED_ICLASS_FNSAVE\r\n#define XED_ICLASS_FSTSW XED_ICLASS_FNSTSW\r\n\r\n#define XED_ICLASS_JMPE XED_ICLASS_INVALID //JMPE is not a valid instructin in XED\r\n#define XED_ICLASS_STOS XED_ICLASS_INVALID //STOS is not a valid instruction in XED\r\n#define XED_ICLASS_LODS XED_ICLASS_INVALID //LODS is not a valid instruction in XED\r\n#define XED_ICLASS_SCAS XED_ICLASS_INVALID //SCAS is not a valid instruction in XED\r\n#define XED_ICLASS_SAL XED_ICLASS_INVALID //SAL is not present in XED\r\n#define XED_ICLASS_SETALC XED_ICLASS_INVALID //SETALC is not present in XED\r\n#define XED_ICLASS_HINT_NOP XED_ICLASS_INVALID //HINT_NOP is no instruction\r\n#define XED_ICLASS_LOADALL XED_ICLASS_INVALID //LOADALL is not a valid instruction\r\n#define XED_ICLASS_FNENI XED_ICLASS_INVALID //FNENI is not a valid instruction in XED\r\n#define XED_ICLASS_FENI XED_ICLASS_INVALID //FENI is not a valid instruction in XED\r\n#define XED_ICLASS_FNDISI XED_ICLASS_INVALID //FNDISI is not a valid instruction in XED\r\n#define XED_ICLASS_FDISI XED_ICLASS_INVALID //FDISI is not a valid instruction in XED\r\n#define XED_ICLASS_FNSETPM XED_ICLASS_INVALID //FNSETPM is not a valid instruction in XED\r\n#define XED_ICLASS_FSETPM XED_ICLASS_INVALID //FSETPM is not a valid instruction in XED\r\n\r\n";
            header += "static instrinfo info[]=\r\n{\r\n";

            //add the instructions to the listbox
            foreach (Instruction curInstr in instrList)
            {
                string test = "";
                test += String.Format("{0,-26}", "XED_ICLASS_" + curInstr.mnemonic);

                header += "    {\r\n        XED_ICLASS_" + curInstr.mnemonic + ",\r\n";

                for (int i = 0; i < curInstr.operands.Count; i++)
                {
                    if (i > 0)
                        test += " , ";
                    string operandStr = "";
                    string a = "UNKNOWN";
                    string t = "unknown";
                    string other = "";
                    if (curInstr.operands[i].bHasAt)
                    {
                        if (curInstr.operands[i].a.Length > 0)
                            a = curInstr.operands[i].a;
                        if (curInstr.operands[i].t.Length > 0)
                            t = curInstr.operands[i].t;
                        operandStr = "a:_" + a + "|t:_" + t;
                    }
                    else
                    {
                        operandStr = curInstr.operands[i].other;
                        other = operandStr;
                        a = "UNKNOWN";
                        t = "other";
                    }
                    header += String.Format("        {{_{0,-8}, _{1,-8}, \"{2}\"}},\r\n", a, t, other);
                    test += String.Format("{0,-20}", operandStr);
                }

                listBox1.Items.Add(test);

                string empty_operand = String.Format("        {{_{0,-8}, _{1,-8}, \"{2}\"}},\r\n", "NONE", "none", "");
                int empty_operands_needed = 4 - curInstr.operands.Count;
                for (int i = 0; i < empty_operands_needed; i++)
                {
                    header += empty_operand;
                }
                header += "    },\r\n";
            }

            //finish the header
            header += "};\r\n";

            label1.Text += ", Number of Instructions: " + listBox1.Items.Count.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string list = "";
            for (int i = 0; i < listBox1.Items.Count; i++)
            {
                list += listBox1.Items[i].ToString() + "\r\n";
            }
            Clipboard.SetText(list);
            MessageBox.Show("List copied!");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(header);
            MessageBox.Show("Header copied!");
        }
    }
}
